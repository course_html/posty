const CracoAlias = require("craco-alias");

module.exports = {
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        source: "options",
        baseUrl: "src", // this is from where all search in files will start
        aliases: {
          "@pages": "./pages",
          "@style": "./assets/style/",
          "@icons": "./assets/icons/",
        }
      }
    }
  ]
};