import { MainLayout } from 'layout/MainLayout'
import { ProductPanel } from 'components/Table/ProductPanel'
import { TableProduct } from 'components/Table/TableProduct'
import { useAppSelector } from 'hooks'
import '@style/page/productPage.scss'
import { Searsh } from 'components/shared/Searsh'
import { Button } from 'components/shared/Button'
import { Box } from 'components/shared/Box'
import { FilteredPanel } from 'components/Table/FilteredPanel'

export const ProductPage = () => {
  const { category } = useAppSelector((state) => state.products)
  const { selectedCategory } = useAppSelector((state) => state.products)
  

  const isChecked = (name: string) =>  selectedCategory ? selectedCategory.includes(name) : false

  return (
    <MainLayout>
      <div className="product-page">
        <div className="page-title">
          <h2 className="product-page__title title-md">Продукты</h2>
        </div>

        <div className="product-page__content">
          <Box flex={true} mb="20px">
            <Searsh />
            <Button icon="remove" text="Добавить категорию" />
          </Box>
          <FilteredPanel
            columns={['Артикул', 'Название', 'Дата обновления', 'Статус']}
          />
          {category.map((i) => {
            return (
              <ProductPanel
                key={i.name}
                name={i.name}
                checkbox={isChecked(i.name)}
              >
                {i.subcategory.map((i) => {
                  return (
                    <div key={i.name}>
                      <ProductPanel
                        name={i.name}
                        checkbox={selectedCategory.includes(i.name)}
                      >
                        <TableProduct data={i.items} />
                      </ProductPanel>
                    </div>
                  )
                })}
              </ProductPanel>
            )
          })}
        </div>
      </div>
    </MainLayout>
  )
}
