import { MainLayout } from "layout/MainLayout";

export const MainPage = () => {
  return (
    <MainLayout>
      <h2>Main</h2>
    </MainLayout>
  );
};