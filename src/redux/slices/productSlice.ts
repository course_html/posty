import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface IInitialState {
  hot: Record<string, boolean>
  checked: Record<string, Record<string, ICategory>>,
  selectedRows: number[],
  selectedCategory: string[],
  category: ICategory[]
}

interface ICategory {
  name: string,
  subcategory: ISubcategory[],
}

interface ISubcategory {
  name: string,
  items: IItems[]
}
interface IItems {
  id: number, status: boolean, name: string, date: string,
}

const initialState: IInitialState = {
  hot: {},
  checked: {},

  selectedRows: [],
  selectedCategory: [],
  category: [
    {
      name: 'Горячее',
      subcategory: [
        {
          name: 'На утро',
          items: [
            { id: 11234345, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 21233434, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 66820076, status: false, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 84534564, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 55522827, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 63123434, status: true, name: 'Кофе', date: '14 июл в 13:45' }
          ]
        },
        {
          name: 'На обед',
          items: [
            { id: 13334345, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 25663434, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 66123376, status: false, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 12231233, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 45645645, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 29384902, status: true, name: 'Кофе', date: '14 июл в 13:45' }
          ]
        }
      ]
    },
    {
      name: 'Холодное',
      subcategory: [
        {
          name: 'На утро',
          items: [
            { id: 11234345, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 21233434, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 66820076, status: false, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 84534564, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 55522827, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 63123434, status: true, name: 'Кофе', date: '14 июл в 13:45' }
          ]
        },
        {
          name: 'На обед',
          items: [
            { id: 11234345, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 21233434, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 66820076, status: false, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 84534564, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 55522827, status: true, name: 'Кофе', date: '14 июл в 13:45' },
            { id: 63123434, status: true, name: 'Кофе', date: '14 июл в 13:45' }
          ]
        }
      ]
    }
  ]
}

export const slice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    changeHot: (state, { payload }) => {
      state.hot[payload] = !state.hot[payload]
    },
    changeCold: (state, { payload }) => {
      state.hot[payload] = !state.hot[payload]
    },
    changeHotAll: (state, { payload }) => {
      state.hot[payload] = !state.hot[payload]
    },
    toggleCheckedProduct: (state, actions: PayloadAction<number>) => {
      // if (state.selectedRows.includes(actions.payload)) {
      //   state.selectedRows = state.selectedRows.filter((i) => i !== actions.payload)
      // } else {
      //   state.selectedRows.push(actions.payload)
      // }
    },
    toggleCheckedCaterory: (state, { payload }: PayloadAction<string>) => {

    //   if (!state.selectedCategory.includes(payload)) {
    //     // добавление
    //     // добавляем в выбранное все чекбоксы
    //     state.category.forEach((category) => {
    //       if (category.name === payload) {
    //         console.log(payload);


    //         category.subcategory.forEach(subcategory => {
    //           // if (sub) {
    //           state.selectedCategory.push(subcategory.name)

    //           // }
    //           // state.selectedCategory.push(subcategory.name)
    //         })
    //       }


    //       // если наша категория равна выбранной категории

    //       // if (category.name === actions.payload) {
    //       //   state.selectedCategory.push(category.name)
    //       //   category.subcategory.forEach(subcategory => {
    //       //     // console.log(subcategory);
    //       //     state.selectedCategory.push()
    //       //   })

    //       // category.subcategory.forEach((subcategory) => {
    //       //   if (subcategory.name === actions.payload) {
    //       //     state.selectedCategory.push(actions.payload)
    //       //   }
    //       // })
    //       // }
    //     })
    //   } else {
    //     state.selectedCategory = state.selectedCategory.filter((i) => i !== payload)

    //   }
    }
  },
})

export const { toggleCheckedProduct, toggleCheckedCaterory, changeHot, changeCold } = slice.actions
export default slice.reducer