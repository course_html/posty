import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit'
import productReducer from '../redux/slices/productSlice'

export const store = configureStore({
	reducer: {
		products: productReducer
  }
})
export type AppThunk = ThunkAction<void, typeof store, unknown, Action<string>>;
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch