import React, { FC, useState } from 'react'
import { CheckBox } from 'components/shared/CheckBox'
import { Arrow } from 'components/shared/Arrow'
import '@style/components/table/productPanel.scss'
import { useAppDispatch } from 'hooks'
// import { selectCheckboxCategory } from 'redux/slices/productSlice'
import { Button } from 'components/shared/Button'
import { toggleCheckedCaterory } from 'redux/slices/productSlice'

interface IProductPanel {
  name: string
  checkbox: boolean
  children: React.ReactNode
}

export const ProductPanel: FC<IProductPanel> = ({ name, children, checkbox }) => {
  const dispach = useAppDispatch()
  const [isVisible, setIsVisible] = useState(false)

  const isVisiblePanel = isVisible ? 'product-panel' : 'product-panel visible'
  const isRoundArrow = isVisible
    ? 'product-panel__arrow'
    : 'product-panel__arrow visible'

  const isCallback = () => {
    dispach(toggleCheckedCaterory(name))
  }

  return (
    <div className={isVisiblePanel}>
      <div className="product-panel__header">
        <div className="product-panel__controls">
          <CheckBox onChange={isCallback} checked={checkbox} />
          <Arrow
            className={isRoundArrow}
            onClick={() => setIsVisible(!isVisible)}
          />
          <h2 className="title-1">{name}</h2>
        </div>
        <Button icon="remove" text="Добавить категорию" />
      </div>
      {children}
    </div>
  )
}
