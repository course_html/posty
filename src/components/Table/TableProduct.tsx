import { FC } from 'react'
import { CheckBox } from 'components/shared/CheckBox'
import { useAppDispatch, useAppSelector } from 'hooks'
import '@style/components/table/tableProduct.scss'
import { Button } from 'components/shared/Button'
import { changeHot, toggleCheckedProduct } from 'redux/slices/productSlice'

interface ITableProduct {
  data: ITableProductRow[]
}

interface ITableProductRow {
  id: number
  name: string
  date: string
  status: boolean
}

export const TableProduct: FC<ITableProduct> = ({ data }) => {
  const dispach = useAppDispatch()
  const selectedCheckbox = useAppSelector(
    (state) => state.products.selectedRows
  )

  const toggleCheckbox = (id: number) => {
    dispach(changeHot(id))
  }

  const hotCheckedMap = useAppSelector((state) => state.products.hot )

  return (
    <table className="table-product">
      <tbody>
        {data.map((row) => {
          return (
            <tr key={row.id}>
              <td className="table-product__column-1">
                <CheckBox
                  onChange={() => toggleCheckbox(row.id)}
                  checked={
                    hotCheckedMap[row.id]
                  }
                />
              </td>
              <td className="table-product__column-2">{row.id}</td>
              <td className="table-product__column-3">{row.name}</td>
              <td className="table-product__column-4">{row.date}</td>
              <td className="table-product__column-5">
                {row.status ? (
                  <Button mode="btn-success" text="Активен" />
                ) : (
                  <Button mode="btn-error" text="не активен" />
                )}
              </td>
              <td className="table-product__column-6">:</td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}
