import React, { FC } from 'react'
import '@style/components/table/filteredPanel.scss'
import { Button } from 'components/shared/Button'

interface IFilteredPanel {
  columns: string[]
}

interface IFilteredPanelItem {
  name: string
}

export const FilteredPanel: FC<IFilteredPanel> = ({ columns }) => {
  return (
    <div className="filtered-panel">
      {columns.map((column) => (
        <FilteredPanelItem key={column} name={column} />
      ))}
    </div>
  )
}

const FilteredPanelItem: FC<IFilteredPanelItem> = ({ name }) => {
  return (
    <div className="filtered-panel__item">
      {name}
      <Button icon="filter"/>
    </div>
  )
}
