import { Route, Routes } from 'react-router-dom'
import { MainPage } from '@pages/MainPage'
import { ProductPage } from '@pages/ProductPage'
import { UsersPage } from '@pages/UsersPage'

export const App = () => {
  return (
    <div className="app">
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/products" element={<ProductPage />} />
        <Route path="/users" element={<UsersPage />} />
      </Routes>
    </div>
  )
}
