import { Link } from 'react-router-dom'
import { AsideLink } from './shared/AsideLink'
import logo from '@icons/logo.svg'
import '@style/components/aside.scss'

export const Aside = () => {
  return (
    <div className="aside">

      <Link className="aside__logo" to="/">
        <img src={logo} alt="logo" />
      </Link>

      <ul className="aside__list">
        <AsideLink link={'/'} text="Рабочий стол" iconId="home"/>
        <AsideLink link={'/products'} text="Продукты" iconId="product"/>
        <AsideLink link={'/orders'} text="Заказы" iconId="home"/>
      </ul>

      <div className="aside__footer">
        <p className="aside__footer-title">Справочный центр</p>
        <p className="aside__footer-phone">8 800 000 0000</p>
        <button className="aside__footer-btn">Нужна помощь</button>
        <p className="aside__footer-c">© Postilla 2022. Все права защищены.</p>
      </div>

    </div>
  )
}
