import '@style/shared/product.scss'
import { FC } from 'react'
import { Link } from 'react-router-dom'

interface IProduct {
  link: string
}

export const Product: FC<IProduct> = ({link}) => {
  return (
    <Link to={link} className="product">
      <div>название</div>
      <div>стоимость</div>
      <div>акция</div>
      <div>категория</div>
    </Link>
  )
}
