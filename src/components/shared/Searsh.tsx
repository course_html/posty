import '@style/shared/search.scss'
import { FC } from 'react'
import { Icon } from './Icon'

interface ISearch {
  mb?: string
}

export const Searsh: FC<ISearch> = ({ mb = '0' }) => {
  return (
    <div className="chared-search" style={{ marginBottom: mb }}>
      <Icon
        className="chared-search__icon"
        iconId="search"
        width={'18'}
        height={'18'}
      />
      <input type="text" />
    </div>
  )
}
