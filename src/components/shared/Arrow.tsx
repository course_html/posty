import { FC } from 'react'
import { Icon } from './Icon'

interface IArrow {
  onClick: () => void
  className: string
}

export const Arrow: FC<IArrow> = ({ onClick, className }) => {
  return (
    <button
      className={className}
      onClick={onClick}
      style={{ cursor: 'pointer' }}
    >
      <Icon width={'24'} height={'24'} iconId="arrow" />
    </button>
  )
}