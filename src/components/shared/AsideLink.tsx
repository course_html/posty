import { FC } from 'react'
import { Link } from 'react-router-dom'
import { Icon } from './Icon'
import '@style/text.scss'

interface IProps {
  link: string
  text: string
  iconId: string
}

export const AsideLink: FC<IProps> = ({ link, text, iconId }) => {
  return (
    <Link style={{display: 'flex', alignItems: 'center'}} to={link}>
      <Icon margin={'18.5px'} iconId={iconId} width="15" height="15" />

      <span className="text-link">{text}</span>
    </Link>
  )
}
