import { FC } from 'react'

interface IBox {
  mb?: string
  className?: string
  children: React.ReactNode
  flex?: boolean
}

export const Box: FC<IBox> = ({ className, children, mb, flex }) => {
  const flexbox = flex ? { display: 'flex', alignItems: 'center', justifyContent: 'space-between' } : ''
  return (
    <div style={{ 
      marginBottom: mb, 
    ...flexbox 
    }} className={className}>
      {children}
    </div>
  )
}
