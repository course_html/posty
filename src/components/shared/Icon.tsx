import { FC } from 'react'
import IconsSVG from '@icons/sprite.svg'

interface IIcon {
  width: string
  height: string
  iconId: string
  xmlns?: string
  margin?: string
  className?: string
}

export const Icon: FC<IIcon> = ({
  className = '',
  width = 24,
  height = 24,
  iconId,
  xmlns = 'http://www.w3.org/2000/svg',
  margin = 0,
}) => {
  return (
    <svg
      className={className}
      style={{ marginRight: margin }}
      xmlns={xmlns}
      width={width}
      height={height}
    >
      <use xlinkHref={`${IconsSVG}#${iconId}`}></use>
    </svg>
  )
}
