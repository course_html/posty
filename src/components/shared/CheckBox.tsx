import React, { FC } from 'react'
import '@style/shared/checkbox.scss'

interface IChekBox {
  checked: boolean
  onChange: any
}

export const CheckBox: FC<IChekBox> = ({ checked, onChange }) => {
  return (
    <label onChange={onChange} className="checkbox-shared">
      <input type="checkbox" checked={checked} />
      <span className="checkmark"></span>
    </label>
  )
}
