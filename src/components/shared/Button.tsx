import { FC } from 'react'
import { Icon } from './Icon'
import '@style/shared/button.scss'

interface IButton {
  icon?: string
  text?: string
  mode?: string
}

export const Button: FC<IButton> = ({ icon, text, mode }) => {
  const styles = mode ? `shared-button ${mode}` : 'shared-button'

  return (
    <button className={styles}>
      {icon && <Icon iconId={icon} width="18" height="18" />}
      <p>{text}</p>
    </button>
  )
}
