import { Header } from 'components/Header'
import { FC, ReactNode } from 'react'
import { Aside } from '../components/Aside'

import '@style/page/page.scss'

interface IMainLayout {
  children: ReactNode
}

export const MainLayout: FC<IMainLayout> = ({ children }) => {
  return (
    <>
      <div className="main">
        <Header />
        <Aside />
        <div className="content">
          {children}
        </div>
      </div>
    </>
  )
}
